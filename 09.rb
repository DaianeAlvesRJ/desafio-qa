require 'test/unit'

class TestPrice < Test::Unit::TestCase
    def price(goods)
        co = CheckOut.new(RULES)
        goods.split(//).each { |item| co.scan(item) }
        co.total 
    end

    def test_totals
        assert_equal(  0, price(""))
        assert_equal( 50, price("A"))
        assert_equal( 80, price("AB"))
        assert_equal(115, price("CDBA"))

        assert_equal(100, price("AA"))
        assert_equal(130, price("AAA"))
        assert_equal(180, price("AAAA"))
        assert_equal(230, price("AAAAA"))
        assert_equal(260, price("AAAAAA"))

        assert_equal(160, price("AAAB"))
        assert_equal(175, price("AAABB"))
        assert_equal(190, price("AAABBD"))
        assert_equal(190, price("DABABA"))
    end

    def test_incremental
        co = CheckOut.new(RULES)
        assert_equal( 0, co.total)
        co.scan("A"); assert_equal( 50, co.total)
        co.scan("B"); assert_equal( 80, co.total)
        co.scan("A"); assert_equal(130, co.total)
        co.scan("A"); assert_equal(160, co.total)
        co.scan("B"); assert_equal(175, co.total)
    end 
end

# Google: ruby associative array
# https://ruby-doc.org/core-2.0.0/Hash.htmli
RULES = {
    'A' => {
        "unit" => 50,
        "special" => [3,130]
    },
    'B' => {
        'unit' => 30,
        'special' => [2,45]
    },
    'C' => {
        'unit' => 20
    },
    'D' => {
        'unit' => 15
    }
}

class CheckOut
    # Google: ruby class constructor method
    # http://www.rubyist.net/~slagell/ruby/objinitialization.html
	def initialize(rules)
        @rules = rules
        # Google: ruby hash
        # https://ruby-doc.org/core-2.0.0/Hash.html
        @items = {}
        # Google: ruby hash key exists default
        # http://stackoverflow.com/questions/22614169/add-something-to-hash-value-if-key-exists
        @items.default_proc = proc { 0 }
	end

    def scan(item)
        # http://stackoverflow.com/questions/22614169/add-something-to-hash-value-if-key-exists
        @items[item] += 1
    end

	def total
        total = 0
        # Google: ruby iterate hash key value
        # http://stackoverflow.com/questions/1227571/how-to-iterate-over-a-hash-in-ruby
        @items.each do |item, quantity|
            # Google: ruby if else syntax
            # http://www.howtogeek.com/howto/programming/ruby/ruby-if-else-if-command-syntax/
            # Google: ruby hash key exists 
            # http://stackoverflow.com/questions/4528506/how-to-check-if-a-specific-key-is-present-in-a-hash-or-not
            if(@rules[item].key?('special'))
                total += (
                    # Google: ruby integer division
                    # http://stackoverflow.com/questions/5502761/why-is-division-in-ruby-returning-an-integer-instead-of-decimal-value
                    (quantity / @rules[item]['special'][0]) * @rules[item]['special'][1]
                ) +
                (
                    # Google: ruby division remainder
                    # http://stackoverflow.com/questions/9086131/how-to-find-remainder-of-a-division-in-ruby
                    (quantity % @rules[item]['special'][0]) * @rules[item]['unit']
                )
            else
                total += @rules[item]['unit'] * quantity
            end
        end
        # Google: ruby return method
        # https://www.tutorialspoint.com/ruby/ruby_methods.htm
        return total
	end
end
