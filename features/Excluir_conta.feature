Feature: Exclusão de conta do WhatsApp
  Como usuario necessita ter uma conta para excluí-la

  Scenario: Exclusão de conta
    Given i press the menu key 
    And i press the "Conta" button
    And i press the "Apagar minha conta" button
    And i enter text "21953210121" into "seu número"
    And i press the "APAGAR MINHA CONTA" button
    And i select "Estou apagando minha conta temporariamente" from "Selecione o motivo"
    And i press the "APAGAR MINHA CONTA" button
    And i see the text "Você deseja realmente apagar tua conta?"
    And i press the "APAGAR MINHA CONTA" button
    Then i see the text "Bem-vindo(a) ao WhatsApp"

