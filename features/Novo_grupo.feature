Feature: Criação de um Novo grupo no WhatsApp
  Como usuário 
  Desejo criar um novo grupo 
  Para conversar com meus amigos

  Scenario: Novo Grupo
    Given i press the menu key 
    And i press the "Novo Grupo" button
    And press list item number 1
    And press the enter button
    And i enter text "Grupo de Teste" into "Digite o nome do grupo aqui"
    And i press the enter button 
    Then i see the text "Você criou o grupo \"Grupo de Teste\""
